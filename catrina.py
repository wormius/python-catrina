# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2018 Gato Wormius
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__version__ = 1.0
__author__ = "Gato Wormius - gatowormius@gmail.com"


import collections

import os

import re

import sqlite3


def create(path):
    i = 0
    path_c = path
    while True:
        if os.path.exists(path_c):
            i += 1
            path_c = "{1} ({0})".format(i, path)
        else:
            break

    if i > 0:
        path = "{1} ({0})".format(i, path)

    conn = sqlite3.connect(path)
    conn.close()

    return os.path.abspath(path)


def validate_value_for_sqlite_type(sqlite_type, value):
    if sqlite_type == 'integer':
        try:
            return int(float(value))
        except ValueError:
            return None

    elif sqlite_type == 'real':
        try:
            return float(value)
        except ValueError:
            return None

    elif sqlite_type == 'text':
        return str(value)

    elif sqlite_type == 'blob':
        return value

    else:
        return value[1](sqlite_type, value[0])


class TextTemplate(object):

    def __init__(self, template):

        self.__variables = collections.OrderedDict()
        self.__occurrences = []

        try:
            with open(template) as file:
                self.__template = file.read()
        except FileNotFoundError:
            self.__template = template
        except OSError:
            self.__template = template


    def compile(self):

        final = self.__template

        for k in self.__variables.keys():
            varname = "{{" + k + ":" + self.__variables[k] + "}}" if self.__variables[
                                                                         k] is not None else "{{" + k + "}}"
            final = final.replace(varname, str(self.__dict__[k]))

        return final

    def get_keys(self):
        return self.__variables.keys()

    def get_occurrences(self):
        return tuple(self.__occurrences)

    def get_types(self):
        return self.__variables

    def __process_template(self, template):
        regex = r"\{\{(\s*[a-zA-Z]\w*\s*)(:{1}(\s*[a-zA-Z]\w*\s*)){0,1}\}\}"
        matches = re.finditer(regex, template, re.MULTILINE)

        for match in enumerate(matches):
            var = match[1].group().replace(" ", "").replace("{{", "").replace("}}", "").split(":")
            self.__dict__[var[0]] = None

            if len(var) > 1:
                self.__variables[var[0]] = var[1]
            else:
                self.__variables[var[0]] = None

            self.__occurrences.append(var[0])

            template = template.replace(match[1].group(), match[1].group().replace(" ", ""))

        return template

    def __setattr__(self, key, value):

        if key == "_TextTemplate__variables" or key == "_TextTemplate__occurrences":
            self.__dict__[key] = value
        elif key == "_TextTemplate__template":
            self.__dict__[key] = self.__process_template(value)
        else:
            if key in self.__variables.keys():
                if self.__variables[key] is None:
                    self.__dict__[key] = value
                else:
                    try:
                        self.__dict__[key] = value[1](self.__variables[key], value[0])
                    except TypeError:
                        raise NotImplementedError(
                            "Expected validation function for type \"{0}\"".format(self.__variables[key]))

            else:
                raise KeyError("\"{0}\" variable is not defined in this template".format(key))

    def __setitem__(self, key, value):
        if key in self.__variables.keys():
            if self.__variables[key] is None:
                self.__dict__[key] = value
            else:
                try:
                    self.__dict__[key] = value[1](value[0])
                except TypeError:
                    raise NotImplementedError(
                        "Expected validation function for type \"{0}\"".format(self.__variables[key]))

        else:
            raise KeyError("\"{0}\" variable is not defined in this template".format(key))

    def __getitem__(self, item):
        if item in self.__variables.keys():
            return self.__dict__[item]
        else:
            raise KeyError("\"{0}\" variable is not defined in this template".format(item))

    def __str__(self):
        return self.__template


class Query(TextTemplate):

    def __init__(self, statement, database, fields=None):

        if not os.path.exists(database):
            raise sqlite3.DatabaseError

        TextTemplate.__init__(self, statement)

        if TextTemplate.compile(self).strip()[-1] != ';':
            raise ValueError("Invalid SQL sentence (missing \";\" at end of line)")
        elif TextTemplate.compile(self).count(';') == 0:
            raise ValueError("Invalid SQL sentence (missing \";\" at end of line)")
        elif TextTemplate.compile(self).count(';') > 1:
            raise ValueError("Invalid SQL sentence (too many \";\")")

        self.database = database
        self.fields = fields

        if self.fields is not None:

            if isinstance(self.fields, str):
                self.fields = (self.fields,)

            self.__row = collections.namedtuple('Row', self.fields)

    def execute(self, fetch_one=False):

        conn = sqlite3.connect(self.database)
        cursor = conn.cursor()

        sql, parametters = self.__precompile()
        rows = cursor.execute(sql, parametters).fetchall()

        conn.commit()
        conn.close()

        if rows is None or len(rows) == 0:
            if fetch_one:
                if self.fields is not None:
                    r = [None for i in self.fields]

                    return self.__row(*r)
                else:
                    return ()
            else:
                return []

        if self.fields is not None:
            if fetch_one:
                rows = self.__row(*rows[0])
            else:
                rows = list(map(self.__row._make, rows))
        else:
            if fetch_one:
                rows = rows[0]

        return rows

    def compile(self):
        data = collections.OrderedDict()
        types = self.get_types()

        for k in self.get_keys():
            data[k] = self[k]

        compiled = str(self)
        for k in data.keys():
            var_tpl = None
            if types[k] is not None:
                var_tpl = ("[[{0}:{1}]]".format(k, types[k]).replace("[", "{").replace("]", "}"))
            else:
                var_tpl = ("[[{0}]]".format(k).replace("[", "{").replace("]", "}"))

            if types[k] == "text":
                compiled = compiled.replace(var_tpl, "\"[[{0}]]\"".format(k).replace("[", "{").replace("]", "}"))
            else:
                compiled = compiled.replace(var_tpl, "[[{0}]]".format(k).replace("[", "{").replace("]", "}"))

        tpl = TextTemplate(compiled)

        for k in data.keys():
            tpl[k] = data[k]

        return tpl.compile()

    def __precompile(self):
        data = collections.OrderedDict()
        types = self.get_types()

        for k in self.get_keys():
            data[k] = self[k]

        precompiled = str(self)
        for k in data.keys():
            var_tpl = None
            if types[k] is not None:
                var_tpl = ("[[{0}:{1}]]".format(k, types[k]).replace("[", "{").replace("]", "}"))
            else:
                var_tpl = ("[[{0}]]".format(k).replace("[", "{").replace("]", "}"))

            precompiled = precompiled.replace(var_tpl, "?")

        occurrences = self.get_occurrences()
        values = []
        for k in occurrences:
            values.append(data[k])

        return precompiled, tuple(values)

    def __setattr__(self, key, value):
        if key is '_TextTemplate__variables' or key is '_TextTemplate__template' or key == "_TextTemplate__occurrences":
            TextTemplate.__setattr__(self, key, value)

        elif key is 'database' or key is 'fields' or key is '_Query__row':
            self.__dict__[key] = value

        elif self.get_types()[key] is not None:
            TextTemplate.__setattr__(self, key, (value, validate_value_for_sqlite_type))

        else:
            TextTemplate.__setattr__(self, key, value)

    def __setitem__(self, key, value):
        if self.get_types()[key] is not None:
            TextTemplate.__setattr__(self, key, (value, validate_value_for_sqlite_type))
        else:
            TextTemplate.__setattr__(self, key, value)


class Script(TextTemplate):

    def __init__(self, statement, database):

        if not os.path.exists(database):
            raise sqlite3.DatabaseError

        self.database = database

        TextTemplate.__init__(self, statement)

        if TextTemplate.compile(self).count(';') < len(self.compile()):
            raise ValueError("Some SQL sentence is invalid (very few \";\")")
        elif TextTemplate.compile(self).count(';') > len(self.compile()):
            raise ValueError("Some SQL sentence is invalid (too many \";\")")

    def execute(self):

        sql_list = str(self).replace("\n", " ").split(";")

        results = []

        for sql in sql_list:
            if sql.replace(" ", "") == "":
                continue

            query = Query("{0};".format(sql.strip()), self.database)

            for k in query.get_keys():
                query[k] = self[k]

            results.append(query.execute())

        return results

    def compile(self):
        sql_list = str(self).replace("\n", " ").split(";")

        results = []

        for sql in sql_list:
            if sql.replace(" ", "") == "":
                continue

            query = Query("{0};".format(sql.strip()), self.database)

            for k in query.get_keys():
                query[k] = self[k]

            results.append(query.compile())

        return results

    def __setattr__(self, key, value):
        if key is '_TextTemplate__variables' or key is '_TextTemplate__template' or key == "_TextTemplate__occurrences":
            TextTemplate.__setattr__(self, key, value)

        elif key is 'database' or key is 'fields' or key is '_Query__row':
            self.__dict__[key] = value

        elif self.get_types()[key] is not None:
            TextTemplate.__setattr__(self, key, (value, validate_value_for_sqlite_type))

        else:
            TextTemplate.__setattr__(self, key, value)

    def __setitem__(self, key, value):
        if self.get_types()[key] is not None:
            TextTemplate.__setattr__(self, key, (value, validate_value_for_sqlite_type))
        else:
            TextTemplate.__setattr__(self, key, value)

# Catrina

El modulo `catrina` es muy útil para llevar a cabo operaciones con la base de datos `sqlite`. El modulo se basa en la implementación de una plantilla de texto con soporte para especificación de tipos de datos y validación de estos.

El módulo `catrina` está pensado para hacer más simple el proceso de crear ORM propios o modelos de datos; y depende sólamente de la la librería estandar de pyhton.

### Ejemplo de uso:


**Script python**
~~~python
import catrina

sql = "select name, email from users where id < {{max_id:integer}};"
database = "database.db"	#Base de datos existente
fields = ("nombre", "correo")

query = catrina.Query(sql, database, fields)
query.max_id = 5

data = query.execute()

for row in data:
	print("Name: {0}, Email: {1}".format(row.nombre, row.correo))
~~~

**Salida del intérprete**
~~~bash
Name: Fulano1, Email: fulano1@mail.com
Name: Fulano2, Email: fulano2@mail.com
Name: Fulano3, Email: fulano3@mail.com
Name: Fulano4, Email: fulano4@mail.com
~~~

### TO DO LIST

- Wiki
	- Tutorial
	- Documentación
- Documentar código
- Hacer posible la creación dinámica de plantillas
